<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/view','SalimController@alim');
Route::get('/index',function(){
    return view('index', ['page' => 'home','mainPage'=> 'dashboard']);
})->name('shopkeeper.index');
Route::get('/categories',function(){
    return view('categories', ['page' => 'categories','mainPage' => 'catalog']);
})->name('shopkeeper.categories');
Route::get('/products',function(){
    return view('products', ['page' => 'products','mainPage' => 'catalog']);
})->name('shopkeeper.products');