
@extends('master')

@section('style')
<style>
.buttonRightShiftCss{
  float: right;
    font-size: 1.1rem;
    font-weight: 400;
    margin: 2px;
}
.gallery{
  width:100%;
  
}
.gallery img{
  width:100px;
  height:100px;
  margin:2%;
}
.deleteButton{
  margin-top: 94px;
margin-left: -37px;
}
}
  </style>
  @endsection
@section('body')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Product Form</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Product Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- <div class="container-fluid"> -->
<!-- <div class="row"> -->
<div class="card card-info">
             
              <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Product Details</h3>
                <!-- <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fas fa-times"></i></button>
              </div> -->
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                <div class="form-group">
                        <label>Product Name:</label>
                        <input type="text" class="form-control" placeholder="Enter Product Name ...">
                      </div>
                <div class="form-group">

                        <label>Product Meta Data:</label>
                        <input type="text" class="form-control" placeholder="Enter Product Meta Data ...">
                      </div>
           
                      <div class="form-group">
                        <label>Size:</label>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox">
                          <label class="form-check-label">Small</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" checked="">
                          <label class="form-check-label">Medium</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" disabled="">
                          <label class="form-check-label">Large</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="checkbox" disabled="">
                          <label class="form-check-label">Extra Large</label>
                        </div>
                      </div>
                <div class="form-group">

                        <label>Price:</label>
                        <input type="text" class="form-control" placeholder="Enter Price ...">
                      </div>
                <div class="form-group">

                        <label>Description:</label>
                        <div class="card-body pad">
              <div class="mb-3">
                <textarea class="textarea" placeholder="Place some text here"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </div>
              
            </div>
                      </div>
                
                
                  <div class="form-group">
                    <label for="exampleInputFile">Image / Video</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" multiple id="gallery-photo-add">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
<div class="gallery"></div>
</div>
                  <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
              <!-- /.card-body -->
            </div>
<!-- </div> -->
      <!-- </div> -->
    </section>
    <section class="content">
      <!-- <div class="container-fluid"> -->
<!-- <div class="row"> -->
 
  <form>
  <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Specification</h3>
                <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fas fa-times"></i></button>
              </div>
                    <button type="button" class="btn btn-info buttonRightShiftCss"><i class="fas fa-video"></i></button>
                <button type="button" class="btn btn-info buttonRightShiftCss"><i class="fas fa-camera"></i></button>
                <button type="button" class="btn btn-info buttonRightShiftCss pluss" onclick="addRow(this)"><i class="fas fa-plus"></i></button>
              </div>
              <div class="card-body">
                <div class="row">
                <div class="col-md-12 col-sm-12 addRoww">

                <div class="row deletewaa">
                <div class="col-md-5 col-sm-12 p-2">
                    <input type="text" class="form-control" placeholder="Enter Name here">
                  </div>
                  <div class="col-md-6 col-sm-12 p-2">
                    <input type="text" class="form-control" placeholder="Enter description here">
                  </div>
                  <div class="col-md-1 col-sm-4 p-2">
                    <button type="button" class="btn btn-danger deleteButtonn" onclick='deleteRow(this)'><i class="fas fa-trash"></i></button>
                  </div>
                </div>
                </div>
                </div>
              </div>
              <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
          
  </form>

    </section>
    <!-- /.content -->
  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
// function readURL(input) {
//         if (input.files && input.files[0]) {
//             var reader = new FileReader();

//             reader.onload = function (e) {
//                 $('#blah')
//                     .attr('src', e.target.result)
//                     .width(150)
//                     .height(200);
//             };

//             reader.readAsDataURL(input.files[0]);
//         }
//     }

$(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                // $(".gallery").append("<span>  $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview); <button type='button' class='btn btn-danger deleteButton' ><i class='fas fa-trash'></i></button></span>");


                  //  var imgg = $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                  //  var imgg = $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);;
                  //  console.log('salim',imgg);
                $(".gallery").append("<span class='deleteImgEach'><img src="+event.target.result+">"+
                "<button type='button' class='btn btn-danger deleteButton' onclick='deleteImgEach(this)'>"+
                "<i class='fas fa-trash'></i></button>"+
                "</span>");
                }

                reader.readAsDataURL(input.files[i]);
            }

        }

    };

    $('#gallery-photo-add').on('change', function() {
        imagesPreview(this, 'div.gallery');
    });
});
function addRow(elm){

  // alert('bantai rapper')
  $(".addRoww").append("<div class='row deletewaa'>"+
                "<div class='col-md-5 col-sm-12 p-2'>"+
                    "<input type='text' class='form-control' placeholder='Enter Name here'>"+
                  "</div>"+
                  "<div class='col-md-6 col-sm-12 p-2'>"+
                    "<input type='text' class='form-control' placeholder='Enter description here'>"+
                  "</div>"+
                  "<div class='col-md-1 col-sm-4 p-2'>"+
                    "<button type='button' class='btn btn-danger deleteButtonn' onclick='deleteRow(this)'><i class='fas fa-trash'></i></button>"+
                  "</div>"+
                "</div>");
}
                  // Remove parent of 'remove' link when link is clicked.
function deleteRow(elm){
    // alert('bantai rapper')
console.log('salim',$(elm).parent().parent().remove());
    // e.preventDefault();

$(elm).parent().parent().remove();
}
function deleteImgEach(elm){
    // alert('bantai rapper')
console.log('salim',$(elm).parent().remove());
    // e.preventDefault();

$(elm).parent().remove();
}
  </script>
  @endsection
@section('script')

@endsection
